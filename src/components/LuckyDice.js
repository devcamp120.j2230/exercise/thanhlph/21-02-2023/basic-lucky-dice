import { Component } from "react";
import dice from "../assets/images/dice.png"
import dice1 from "../assets/images/dice1.png"
import dice2 from "../assets/images/dice2.png"
import dice3 from "../assets/images/dice3.png"
import dice4 from "../assets/images/dice4.png"
import dice5 from "../assets/images/dice5.png"
import dice6 from "../assets/images/dice6.png"


class LuckyDice extends Component {
    constructor(props) {
        super(props);
        this.state = {
            diceNumber: 0,
            src: dice
        }
    }

    onButtonclickHandler = (event) => {
        const diceRandom = 1 + Math.floor(Math.random() * 6);

        this.setState({
            diceNumber: diceRandom
        })

        if (diceRandom === 1) {
            this.setState({
                src: dice1
            })
        }
        if (diceRandom === 2) {
            this.setState({
                src: dice2
            })
        }
        if (diceRandom === 3) {
            this.setState({
                src: dice3
            })
        }
        if (diceRandom === 4) {
            this.setState({
                src: dice4
            })
        }
        if (diceRandom === 5) {
            this.setState({
                src: dice5
            })
        }
        if (diceRandom === 6) {
            this.setState({
                src: dice6
            })
        }
    }
    render() {
        return (
            <div>
                <h3>Lucky Dice</h3>
                <button onClick={this.onButtonclickHandler}>Ném</button>
                <img src={this.state.src} style={{ width: "300px" }}></img>
                <p>Dice: {this.state.diceNumber}</p>
            </div>
        )
    }
}

export default LuckyDice;